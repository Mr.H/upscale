
package mh.upscale;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.imageio.ImageIO;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;

import java.awt.image.BufferedImage;
import java.awt.Color;

public class App {
    public static void main(String[] args) {
        try {
            // create Options object
            Options options = new Options();
            options.addOption("f", "file", true, "Path to the file that will be upscaled");
            options.addOption("c", "cycles", true, "Number of cycles used to upscale");
            options.addOption("p", "postfix", true, "Postfix to append to upscaled filename");
            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(options, args);
            if (!cmd.hasOption('f')) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("upscale", options);
                return;
            }
            String postfix = cmd.hasOption('p') ? cmd.getOptionValue('p') : "_high_res";
            int cycles = cmd.hasOption('c') ? Integer.valueOf(cmd.getOptionValue('c')) : 1;
            String inputFileName = cmd.getOptionValue('f');
            String outputFileName = new StringBuilder()
                    // original fileName
                    .append(inputFileName.substring(0, inputFileName.lastIndexOf('.')))
                    // postfix
                    .append(postfix)
                    // fileExtension
                    .append(inputFileName.substring(inputFileName.lastIndexOf('.'))).toString();
            File inputFile = new File(inputFileName);
            File outputfile = new File(outputFileName);
            System.out.println(String.format("Reading file %s", inputFile.toPath().toAbsolutePath()));
            BufferedImage lowResImage = ImageIO.read(inputFile);
            BufferedImage highResImage = null;
            for (int i = 1; i <= cycles; i++) {
                highResImage = new BufferedImage(lowResImage.getWidth() * 2, lowResImage.getHeight() * 2,
                        BufferedImage.TYPE_INT_RGB);
                // @formatter:off                    
                ProgressBarBuilder pbb = new ProgressBarBuilder()
                    .setInitialMax(highResImage.getWidth() * highResImage.getHeight())
                    .setStyle(ProgressBarStyle.COLORFUL_UNICODE_BLOCK)
                    .setTaskName(String.format("upscale cycle %d",i))
                    .setUnit("px", 1)
                    .setUpdateIntervalMillis(50)
                    .showSpeed();
                // @formatter:on
                try (ProgressBar pb = pbb.build()) {
                    for (int x = 0; x < highResImage.getWidth(); x++) {
                        for (int y = 0; y < highResImage.getHeight(); y++) {
                            // calculate old index
                            int lowResX = x / 2;
                            int lowResY = y / 2;
                            List<Color> surroundingColors = new ArrayList<Color>();
                            surroundingColors.add(getColor(lowResX, lowResY, lowResImage));
                            // find quadrant
                            if (x % 2 == 0 && y % 2 == 0) {
                                // top left
                                surroundingColors.add(getColor(lowResX - 1, lowResY, lowResImage));
                                surroundingColors.add(getColor(lowResX - 1, lowResY - 1, lowResImage));
                                surroundingColors.add(getColor(lowResX, lowResY - 1, lowResImage));
                            }
                            if (x % 2 == 1 && y % 2 == 0) {
                                // top right
                                surroundingColors.add(getColor(lowResX + 1, lowResY, lowResImage));
                                surroundingColors.add(getColor(lowResX + 1, lowResY - 1, lowResImage));
                                surroundingColors.add(getColor(lowResX, lowResY - 1, lowResImage));
                            }
                            if (x % 2 == 1 && y % 2 == 1) {
                                // bottom right
                                surroundingColors.add(getColor(lowResX + 1, lowResY, lowResImage));
                                surroundingColors.add(getColor(lowResX + 1, lowResY + 1, lowResImage));
                                surroundingColors.add(getColor(lowResX, lowResY + 1, lowResImage));
                            }
                            if (x % 2 == 0 && y % 2 == 1) {
                                // bottom left
                                surroundingColors.add(getColor(lowResX - 1, lowResY, lowResImage));
                                surroundingColors.add(getColor(lowResX - 1, lowResY + 1, lowResImage));
                                surroundingColors.add(getColor(lowResX, lowResY + 1, lowResImage));
                            }
                            // calculate average RGB
                            int averageRed = (int) Math.round(surroundingColors.stream().filter(Objects::nonNull)
                                    .mapToInt(Color::getRed).average().getAsDouble());
                            int averageGreen = (int) Math.round(surroundingColors.stream().filter(Objects::nonNull)
                                    .mapToInt(Color::getGreen).average().getAsDouble());
                            int averageBlue = (int) Math.round(surroundingColors.stream().filter(Objects::nonNull)
                                    .mapToInt(Color::getBlue).average().getAsDouble());
                            Color highResColor = new Color(averageRed, averageGreen, averageBlue);
                            highResImage.setRGB(x, y, highResColor.getRGB());
                            pb.step();
                        }
                    }
                }
                lowResImage = highResImage;
            }
            System.out.println(String.format("Writing file %s", outputfile.toPath().toAbsolutePath()));
            ImageIO.write(highResImage, "jpg", outputfile);
            System.out.println("DONE");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static Color getColor(int x, int y, BufferedImage image) {
        if (x >= 0 && x < image.getWidth() && y >= 0 && y < image.getHeight()) {
            return new Color(image.getRGB(x, y));
        } else {
            return null;
        }
    }
}
